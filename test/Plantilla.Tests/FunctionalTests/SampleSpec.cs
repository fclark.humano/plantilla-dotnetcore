﻿using Machine.Specifications;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Text;
using Plantilla.Domain.Enumerations;

namespace Plantilla.Infraestructure.Services
{
    [Subject("Autenticacion")]
    class Cuando_se_autentica_un_Usuario
    {
        static ISecurityService subject;
        static UserToken user_token;
        static UserToken expected_token;

        Establish context = () =>
        {
            subject = A.Fake<ISecurityService>();
            expected_token = new UserToken(sessionId: "1", token: Guid.NewGuid().ToString());
            expected_token.AgregarPermiso(Permiso.IniciarSesion);
            A.CallTo(() => subject.Autenticar("username", "password")).Returns(expected_token);
        };

        Because of = () =>
            user_token = subject.Autenticar("username", "password");

        It deberia_indicar_que_el_usuario_tiene_permisos = () =>
            user_token.Permisos.ShouldNotBeEmpty();

        It deberia_indicar_que_el_usuario_tiene_permiso_IniciarSesion = () =>
            user_token.TienePermiso(Permiso.IniciarSesion).ShouldBeTrue();

        It deberia_tener_un_SessionId_Unico = () =>
            user_token.SessionId.ShouldNotBeNull();
    }
}
