﻿using Plantilla.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Plantilla.Domain.Enumerations
{
    public class Permiso : Enumeration
    {
        public static Permiso IniciarSesion = new Permiso(1, nameof(IniciarSesion));

        public Permiso(int id, string name) : base(id, name)
        {
        }
    }
}
