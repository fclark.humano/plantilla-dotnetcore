﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Plantilla.Infraestructure.Services
{
    public interface ISecurityService
    {
        UserToken Autenticar(string v1, string v2);
    }
}
