﻿using Plantilla.Domain.Enumerations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Plantilla.Infraestructure.Services
{
    public class UserToken
    {
        private ISet<Permiso> _permisos = new HashSet<Permiso>();
        private string _token;
        private string _sessionId;
        public UserToken(string sessionId, string token)
        {
            _sessionId = sessionId;
            _token = token;
        }

        public string SessionId { get { return _sessionId;  } }

        public string NombreUsuario { get; set; }

        public IEnumerable<Permiso> Permisos {
            get { return _permisos; } 
        }
        public void AgregarPermiso(Permiso permiso)
        {
            _permisos.Add(permiso);
        }

        public bool TienePermiso(Permiso permiso)
        {
            return _permisos.Contains(permiso);
        }
    }
}
