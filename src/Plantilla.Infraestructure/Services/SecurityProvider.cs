﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Plantilla.Infraestructure.Services
{
    public class SecurityProvider : ISecurityService
    {
        public UserToken Autenticar(string usuario, string password)
        {
            //Implementar esta funcionalidad
            return new UserToken(usuario, password);
        }
    }
}
